import logging
import time
from typing import List, Optional, Dict

from exasol.client import ExasolClient, ExasolClientExcpetion
from exasol.db_client import ExasolQueryClient, ExasolQueryClientError
from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException

# configuration variables
KEY_PERSONAL_ACCESS_TOKEN = '#personal_access_token'
KEY_ACCOUNT_ID = 'account_id'
KEY_HOST = "host"
KEY_CLUSTER_ACTION = "cluster_action"

REQUIRED_PARAMETERS = [KEY_PERSONAL_ACCESS_TOKEN, KEY_ACCOUNT_ID, KEY_HOST, KEY_CLUSTER_ACTION]
REQUIRED_IMAGE_PARS = []

WAIT_BETWEEN_STATUS_SECONDS = 10
MAX_CONNECTION_TIME_SECONDS = 600


class Component(ComponentBase):
    def __init__(self):
        super().__init__()

    def run(self):
        self.get_input_file_definitions_grouped_by_name()
        self.validate_configuration_parameters(REQUIRED_PARAMETERS)
        self.validate_image_parameters(REQUIRED_IMAGE_PARS)

        params = self.configuration.parameters

        personal_access_token = params.get(KEY_PERSONAL_ACCESS_TOKEN)
        account_id = params.get(KEY_ACCOUNT_ID)
        host = params.get(KEY_HOST)

        self.validate_config(personal_access_token, account_id, host)

        cluster_action = params.get(KEY_CLUSTER_ACTION)

        exasol_client = ExasolClient(personal_access_token=personal_access_token, account_id=account_id)

        clusters = self.get_clusters(host, exasol_client)

        if cluster_action == "Start":
            self.start_clusters(clusters, exasol_client)
            main_cluster = clusters.get("main")
            self.test_connection(main_cluster, account_id, personal_access_token)

        if cluster_action == "Stop":
            self.stop_cluster(clusters, exasol_client)

    def start_clusters(self, clusters: Dict, exasol_client: ExasolClient) -> None:
        main_cluster = clusters.get("main")
        self.start_cluster(main_cluster, exasol_client)

        if clusters.get("child"):
            logging.info("Main cluster started, now starting the specified cluster")
            child_cluster = clusters.get("child")
            self.start_cluster(child_cluster, exasol_client)

    def start_cluster(self, cluster: Dict, exasol_client: ExasolClient) -> None:
        if cluster.get("status") == "running":
            logging.info("Cluster is already running")
        else:
            database_id = cluster.get("databaseId")
            cluster_id = cluster.get("id")
            self._start_cluster(exasol_client, database_id, cluster_id)

    @staticmethod
    def test_connection(main_cluster: Dict, account_id: str, personal_access_token: str) -> None:
        logging.info("Testing Cluster availability")
        dsn = main_cluster.get("dns")
        db_client = ExasolQueryClient(user=account_id, dns=dsn, token=personal_access_token, schema="")
        try:
            db_client.login()
            result = db_client.test_connection()
        except ExasolQueryClientError as e:
            raise UserException("Cluster is unresponsive even after it has been started") from e
        logging.debug(result)
        logging.info("Cluster is available")

    def stop_cluster(self, clusters: Dict, exasol_client: ExasolClient) -> None:
        if clusters.get("isMain"):
            cluster = clusters.get("main")
        else:
            cluster = clusters.get("child")

        cluster_database_id = cluster.get("databaseId")
        cluster_cluster_id = cluster.get("id")
        if cluster.get("status") == "stopped":
            logging.info("Cluster is already stopped")
        else:
            self.check_if_child_clusters_are_on(exasol_client, cluster_database_id)
            self._stop_cluster(exasol_client, cluster_database_id, cluster_cluster_id)

    @staticmethod
    def validate_config(personal_access_token: str, account_id: str, host: str) -> None:
        if not personal_access_token:
            raise UserException("You must enter a valid personal access token")
        if not account_id:
            raise UserException("You must enter a valid account id")
        if not host:
            raise UserException("You must enter a valid host")

    def get_clusters(self, host: str, exasol_client: ExasolClient) -> Dict:
        cluster_list = self.get_all_clusters(exasol_client)
        clusters = {}
        for cluster in cluster_list:
            if cluster["dns"] == host:
                if not cluster.get("mainCluster"):
                    clusters["child"] = cluster
                    clusters["isMain"] = False
                    clusters["main"] = self.get_main_cluster_of_database(cluster.get("databaseId"), exasol_client)
                elif cluster.get("mainCluster"):
                    clusters["main"] = cluster
                    clusters["isMain"] = True
        if not clusters.get("main"):
            raise UserException(
                "Could not find cluster for given host, "
                "please make sure your cluster host and Account ID are filled in correctly")
        return clusters

    def get_main_cluster_of_database(self, database_id: str, exasol_client: ExasolClient) -> Dict:
        cluster_list = self.get_all_clusters(exasol_client)
        for cluster in cluster_list:
            if cluster.get("databaseId") == database_id and cluster.get("mainCluster"):
                return cluster

    def get_cluster_status(self, exasol_client: ExasolClient, cluster_id: str) -> Optional[Dict]:
        cluster_list = self.get_all_clusters(exasol_client)
        for cluster in cluster_list:
            if cluster["id"] == cluster_id:
                return cluster["status"]

    def get_all_clusters(self, exasol_client: ExasolClient) -> List:
        database_list = self.get_database_info(exasol_client)
        cluster_list = []
        for database in database_list:
            database_id = database.get("id")
            cluster_list.extend(self.get_cluster_info(exasol_client, database_id))
        return cluster_list

    @staticmethod
    def get_database_info(exasol_client: ExasolClient) -> List:
        try:
            return exasol_client.get_database_info()
        except ExasolClientExcpetion as exa_exc:
            raise UserException(exa_exc) from exa_exc

    @staticmethod
    def get_cluster_info(exasol_client: ExasolClient, database_id: str) -> Dict:
        try:
            return exasol_client.get_cluster_info(database_id)
        except ExasolClientExcpetion as exa_exc:
            raise UserException(exa_exc) from exa_exc

    def check_if_child_clusters_are_on(self, exasol_client: ExasolClient, database_id):
        cluster_list = self.get_all_clusters(exasol_client)
        for cluster in cluster_list:
            if cluster.get("databaseId") == database_id and not cluster.get("mainCluster"):
                if cluster.get("status") != "stopped":
                    raise UserException("Cannot stop specified cluster, as non main cluster is still running."
                                        "Please stop non main cluster before stopping a main cluster")

    def _start_cluster(self, exasol_client: ExasolClient, database_id: str, cluster_id: str, counter: int = 0) -> None:
        try:
            exasol_client.start_cluster(database_id, cluster_id)
        except ExasolClientExcpetion as exa_exc:
            raise UserException(exa_exc) from exa_exc
        time.sleep(10)
        counter = counter
        exasol_cluster_on = False
        while not exasol_cluster_on:
            cluster_status = self.get_cluster_status(exasol_client, cluster_id)
            if cluster_status == "error":
                raise UserException("An error occurred, please contact Exasol support")
            elif cluster_status not in ["starting", "stopping", "tostop", "tostart", "creating", "stopped", "running"]:
                raise UserException(f"Could not start cluster, unknown status \"{cluster_status}\"")
            if cluster_status == "stopped" and counter > 100:
                logging.info("Cluster is still stopped, retrying to start")
                exasol_client.start_cluster(database_id, cluster_id, counter=counter)
            if cluster_status == "running":
                logging.info("Cluster is on")
                exasol_cluster_on = True
            time.sleep(WAIT_BETWEEN_STATUS_SECONDS)
            counter += WAIT_BETWEEN_STATUS_SECONDS
            if counter % 30 == 0 and counter > 0:
                logging.info(f"Current status : {cluster_status}")
            if counter > MAX_CONNECTION_TIME_SECONDS:
                raise UserException(
                    f"Could not start cluster after {MAX_CONNECTION_TIME_SECONDS} seconds, please try again")
        logging.info(f"took {counter} seconds")

    def _stop_cluster(self, exasol_client: ExasolClient, database_id: str, cluster_id: str) -> None:
        exasol_client.stop_cluster(database_id, cluster_id)
        time.sleep(1)
        counter = 0
        exasol_cluster_on = True
        while exasol_cluster_on:
            cluster_status = self.get_cluster_status(exasol_client, cluster_id)
            if cluster_status == "error":
                raise UserException("An error occurred, please contact Exasol support")
            if cluster_status not in ["starting", "stopping", "tostop", "tostart", "creating", "stopped", "running",
                                      "error"]:
                raise UserException(f"Could not stop cluster, unknown status \"{cluster_status}\"")
            if cluster_status == "running":
                logging.info("Cluster is still on, retrying to stop")
                exasol_client.stop_cluster(database_id, cluster_id)
            if cluster_status == "stopped":
                logging.info("Cluster is off")
                exasol_cluster_on = False
            time.sleep(WAIT_BETWEEN_STATUS_SECONDS)
            counter += WAIT_BETWEEN_STATUS_SECONDS
            if counter % 100 == 0 and counter > 0:
                logging.info(f"Current status : {cluster_status}")
            if counter > MAX_CONNECTION_TIME_SECONDS:
                raise UserException(
                    f"Could not stop cluster after {MAX_CONNECTION_TIME_SECONDS} seconds, please try again")


if __name__ == "__main__":
    try:
        comp = Component()
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
