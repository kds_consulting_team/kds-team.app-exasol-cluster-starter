from keboola.http_client import HttpClient
from requests.exceptions import HTTPError, JSONDecodeError
from typing import List, Dict

BASE_URL = "https://cloud.exasol.com/api"
API_VERSION = "v1/"


class ExasolClientExcpetion(Exception):
    pass


class ExasolClient(HttpClient):
    def __init__(self, personal_access_token=None, account_id=None) -> None:
        self.personal_access_token = personal_access_token
        self.account_id = account_id
        base_url = "/".join([BASE_URL, API_VERSION])
        auth_header = {"Accept": "application/json", "Authorization": f"Bearer {self.personal_access_token}"}
        super().__init__(base_url, auth_header=auth_header)

    def get_database_info(self) -> List[Dict]:
        databases_endpoint = f"accounts/{self.account_id}/databases"
        return self._get_and_process_output(databases_endpoint)

    def start_cluster(self, database_id: str, cluster_id: str) -> None:
        start_cluster_endpoint = f"accounts/{self.account_id}/databases/{database_id}/clusters/{cluster_id}/start"
        self._put_raw_and_process_output(start_cluster_endpoint, database_id, cluster_id)

    def stop_cluster(self, database_id: str, cluster_id: str) -> None:
        stop_cluster_endpoint = f"accounts/{self.account_id}/databases/{database_id}/clusters/{cluster_id}/stop"
        self._put_raw_and_process_output(stop_cluster_endpoint, database_id, cluster_id)

    def get_cluster_info(self, database_id: str) -> List[Dict]:
        cluster_endpoint = f"accounts/{self.account_id}/databases/{database_id}/clusters"

        try:
            cluster_list = self.get(endpoint_path=cluster_endpoint)
        except HTTPError as http_error:
            raise ExasolClientExcpetion(http_error) from http_error
        for i, cluster in enumerate(cluster_list):
            cluster_id = cluster.get("id")
            cluster_dns = self._get_cluster_dns(cluster_id, database_id)
            cluster_list[i]["dns"] = cluster_dns
            cluster_list[i]["databaseId"] = database_id

        return cluster_list

    def _get_cluster_dns(self, cluster_id: str, database_id: str) -> str:
        cluster_connect_endpoint = f"accounts/{self.account_id}/databases/{database_id}" \
                                   f"/clusters/{cluster_id}/connect"
        try:
            cluster_connect_dns_response = self.get(endpoint_path=cluster_connect_endpoint)
        except HTTPError as http_error:
            raise ExasolClientExcpetion(http_error) from http_error
        return dict(cluster_connect_dns_response).get("dns")

    def _get_and_process_output(self, endpoint: str) -> List[Dict]:
        try:
            data = self.get(endpoint_path=endpoint)
        except HTTPError as http_error:
            if http_error.response.status_code == 401:
                raise ExasolClientExcpetion(
                    "Could not authorize, invalid Personal Access Token, please enter a valid one") from http_error
            elif http_error.response.status_code not in [200, 201, 202, 204]:
                raise ExasolClientExcpetion(
                    f"Error occured during get operation on endpoint {endpoint}."
                    f" Message from server {http_error.response.text}.") from http_error
            else:
                raise ExasolClientExcpetion(http_error) from http_error
        except JSONDecodeError as e:
            raise ExasolClientExcpetion(
                f"Failed to get data from endpoint {endpoint}: Please check your Account Id and PAT") from e

        return data

    def _put_raw_and_process_output(self, endpoint: str, database_id: str, cluster_id: str) -> None:
        try:
            response = self.put_raw(endpoint_path=endpoint)
        except HTTPError as http_error:
            raise ExasolClientExcpetion(http_error) from http_error
        if response.status_code == 403:
            raise ExasolClientExcpetion(f"Error occured during put operation on endpoint {endpoint}. "
                                        f"Message from server {response.text}. "
                                        f"Make sure your PAT token is set to databases:use AND databases:operate ")
        elif response.status_code not in [200, 201, 202, 204]:
            raise ExasolClientExcpetion(f"Error occured during put operation on endpoint {endpoint}."
                                        f" Message from server {response.text}. "
                                        f"Contact Exasol support. Cluster : {cluster_id}, database : {database_id}")
