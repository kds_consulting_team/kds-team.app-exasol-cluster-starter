import pyexasol
import logging
from retry import retry
from pyexasol.exceptions import ExaConnectionDsnError, ExaRequestError, ExaConnectionFailedError, ExaQueryError
from pyexasol.statement import ExaStatement
from typing import List

TABLE_DESC_COLUMN_NAME_POSITION = 3


class ExasolQueryClientError(Exception):
    pass


class ExasolQueryClient:
    def __init__(self, user: str, schema: str, dns: str, token=None) -> None:
        self.logger = logging.getLogger("exasol_logger")
        self.dns = dns
        self.user = user
        self.token = token
        self.schema = schema

    @retry(tries=10, delay=15)
    def login(self) -> None:
        try:
            if self.token:
                self.client = pyexasol.connect(dsn=self.dns,
                                               user=self.user,
                                               refresh_token=self.token,
                                               protocol_version=3,
                                               schema=self.schema,
                                               encryption=True)
            else:
                raise ExasolQueryClientError(
                    "You must specify a Personal Access Token or Username and Password to log into Exasol")
        except ExaConnectionDsnError as connection_exc:
            raise ExasolQueryClientError(connection_exc.message) from connection_exc
        except ExaConnectionFailedError as connection_exc:
            raise ExasolQueryClientError(f"{connection_exc.message}. Please recheck port number and that your "
                                         f"Exasol database/cluster is on") from connection_exc
        except ExaRequestError as auth_error:
            raise ExasolQueryClientError("Cluster still unresponsive") from auth_error

    def execute_query(self, query: str) -> ExaStatement:
        try:
            return self.client.execute(query)
        except ExaQueryError as query_error:
            raise ExasolQueryClientError(f"{query_error.message}. {query_error.query}") from query_error

    @retry(tries=10, delay=15)
    def test_connection(self) -> List:
        query = """
        SELECT * FROM EXA_ALL_SCHEMAS
        """
        self.logger.debug(f"running query {query}")
        results = self.execute_query(query)
        results = results.fetchall()
        return results
