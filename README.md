Exsaol Cluster Starter
=============

This component enables you to start and stop Exasol clusters for databases.

**Table of contents:**

[TOC]

Prerequisites
=============

To use this component you need to get a personal access token (PAT) through the UI of [cloud Exasol](https://cloud.exasol.com)
as well as the host of the cluster that you wish to start. 

To get the PAT go to the top left of the cloud exasol site and click on the icon with your initials, click Personal 
Access Tokens. Click create token and generate a new token with Database : use & operate privilages. 


Configuration
=============

- Personal Access Token (#personal_access_token) - [REQ] PAT with use & operate privilages on the database
  via tools"
- Host (host) - [REQ] Host link eg. xyz.xyz.exasol.com
- Account Id (account_id) - [REQ] 
- Cluster Action - [REQ] "Start" or "Stop"

Sample configuration
=============

```json
{
  "parameters": {
    "account_id": "YOUR ACCOUNT",
    "#personal_access_token": "YOUR PAT",
    "host": "host_string_here.clusters.exasol.com",
    "cluster_action": "Start"
  }
}
```

Development
-----------

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path in
the `docker-compose.yml` file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clone this repository, init the workspace and run the component with following command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone https://bitbucket.org/kds_consulting_team/kds-team.app-exasol-cluster-starter/src/master/ kds-team.app-exasol-cluster-starter
cd kds-team.app-exasol-cluster-starter
docker-compose build
docker-compose run --rm dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run the test suite and lint check using this command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose run --rm test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Integration
===========

For information about deployment and integration with KBC, please refer to the
[deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/)
